//
//  SceneDelegate.swift
//  Module-2
//
//  Created by Reinder de Vries on 18/11/2019.
//  Copyright © 2019 LearnAppMaking. All rights reserved.
//

import Foundation

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions)
    {
        if let windowScene = scene as? UIWindowScene {
            
            let newsVC:NewsTableViewController = NewsTableViewController()
            newsVC.title = "News"
            
            let window = UIWindow(windowScene: windowScene)
            
            let navigation = UINavigationController(rootViewController: newsVC)
            window.rootViewController = navigation

            self.window = window
            window.makeKeyAndVisible()
        }
    }

    func sceneDidDisconnect(_ scene: UIScene) {
    
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        
    }

    func sceneWillResignActive(_ scene: UIScene) {
        
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        
    }
}
