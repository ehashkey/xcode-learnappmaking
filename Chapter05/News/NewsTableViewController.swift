//
//  NewsTableViewController.swift
//  Module-2
//
//  Created by Reinder de Vries on 30-03-15.
//  Copyright (c) 2015 LearnAppMaking. All rights reserved.
//

import UIKit
import RealmSwift

class NewsTableViewController: UITableViewController
{
    var articles: Results<Article>
    {
        get {
            let realm = try! Realm()

            return realm.objects(Article.self)
        }
    }

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(onArticlesReceived(notification:)), name: API.articlesReceivedNotification, object: nil)
        API.sharedInstance.requestArticles()
    }
    
    @objc func onArticlesReceived(notification: Notification)
    {
        self.tableView.reloadData()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return articles.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        // Dequeue a table view cell
        var cell:UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: "cellIdentifier")
        
        // If there's no cell, create one
        if(cell == nil) {
            cell = UITableViewCell(style:UITableViewCell.CellStyle.subtitle, reuseIdentifier:"cellIdentifier")
        }
        
        // Set the text and detail text
        let article = articles[indexPath.row]

        cell!.textLabel?.text = article.title
        cell!.detailTextLabel?.text = article.excerpt
        
        return cell!
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let detailVC:NewsDetailViewController = NewsDetailViewController(nibName: "NewsDetailViewController", bundle:nil)
        
        detailVC.article = articles[indexPath.row]

        navigationController?.pushViewController(detailVC, animated:true)
    }
}
