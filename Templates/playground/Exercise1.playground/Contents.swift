//: Playground - noun: a place where people can play

import Foundation

// The variable phi is a double, because 1.618033 is a number with a decimal fraction
var phi:Double = 1.618033

// The variable room is an integer, because 101 is a whole number
var room:Int = 101

print(phi)
print(room)

// Three values a, b, c
let a:Int = 3
let b:Int = 4
let c:Int = a + b

print(c)

// The variable name is a string
var name = "Arthur Dent"
var greeting = "Mighty fine day to you, \(name)!"

print(name)
print(greeting)

// The function product takes two parameters a and b, and multiplies them with each other (a "product")
func product(a:Int, b:Int) -> Int
{
    return a * b
}

// The function power takes one input, and uses the product function to calculate c * c, or "c to the power of 2"
func power(c:Int) -> Int
{
    return product(a: c, b: c)
}

let result_1 = product(a: 3, b: 5)
print(result_1) // Output: 15

let result_2 = power(c: 12)
print(result_2) // Output: 144
