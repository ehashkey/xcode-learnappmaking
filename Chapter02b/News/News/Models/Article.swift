//
//  Article.swift
//  News
//
//  Created by Eric Haschke on 30.12.19.
//  Copyright © 2019 Klunscher. All rights reserved.
//

import Foundation


struct Article
{
    var title: String
    var author: String
    var url: String
    
    var urlRequest: URLRequest {
        URLRequest(url: URL(string: url)!)
    }
}
