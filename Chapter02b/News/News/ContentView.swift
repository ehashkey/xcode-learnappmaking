//
//  ContentView.swift
//  News
//
//  Created by Eric Haschke on 30.12.19.
//  Copyright © 2019 Klunscher. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
